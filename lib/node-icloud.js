'use strict'

const setup = require('./modules/setup.js')
const Contacts = require('./modules/contacts.js')

class NodeICloud {
  constructor(appleID, password) {
    this._appleID = appleID

    return setup(appleID, password)
      .then(response => {
        this.contactsService = new Contacts(response.webservices.contacts.url)
        return this.contactsService.setup()
      })
      .then(() => {
        return this.contactsService.addContact({
          contactId: '0',
          firstName: 'Fake2',
          lastName: 'Contact2',
          isCompany: false,
          phones: [
            {
              label: 'MOBILE',
              field: '555-5555'
            }
          ]
        })
      })
  }
}


module.exports = function(appleID, password) {
  return new NodeICloud(appleID, password)
}
