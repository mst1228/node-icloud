'use strict'

const request = require('request')

class APIRequest {
  constructor() {
    this._jar = request.jar()
  }

  _doRequest(method, url, params, body) {
    return new Promise((resolve, reject) => {
      request({
        method: method,
        url: url,
        jar: this._jar,
        qs: params,
        json: true,
        body: body,
        headers: {
          'Origin': 'https://www.icloud.com',
          'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36',
          'Referer': 'https://www.icloud.com/'
        }
      }, (error, response, body) => {
        if (error) {
          return reject(new RequestError())
        }

        if (response.statusCode !== 200) {
          return reject(new ResponseError(response))
        }

        return resolve(body)
      })
    })
  }
}

const sharedInstance = new APIRequest()

function RequestError() {
  Error.call(this)
  this.message = "API request error"
}

function ResponseError(response) {
  Error.call(this)
  this.message = "API response error"
  this.statusCode = response.statusCode
  this.body = response.body
}

exports.get = function(url, params, body) {
  return sharedInstance._doRequest('GET', url, params, body)
}

exports.post = function(url, params, body) {
  return sharedInstance._doRequest('POST', url, params, body)
}
