'use strict'

const request = require('../core/api-request.js')

module.exports = function(appleID, password) {
  return request.post('https://setup.icloud.com/setup/ws/1/login', {}, {
    apple_id: appleID,
    password: password
  })
}
