'use strict'

const request = require('../core/api-request.js')

class Contacts {
  constructor(url) {
    this._url = url
    this._syncToken = null
    this._prefToken = null
  }

  setup() {
    return request.get(`${this._url}/co/startup`, {locale: 'en_US', order: 'last,first'}, {})
      .then(response => {
        this._syncToken = response.syncToken
        this._prefToken = response.prefToken
      })
  }

  addContact(contact) {
    return this.addContacts([contact])
  }

  addContacts(contacts) {
    return request.post(`${this._url}/co/contacts/card`, {prefToken: this._prefToken, syncToken: this._syncToken}, {contacts: contacts})
  }
}

module.exports = Contacts
